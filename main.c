#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ncurses.h>
#include <time.h>

#define BorderCharacter " "
#define BlankCharacter " "
#define SnakeBodyUD "|"
#define SnakeBodyLR "-"
#define SnakeHead "o"
#define SnakeSpeed 4
#define AddLen 3
#define FruitChar "o"

typedef enum ScreenObjectsEnum {Clear = 0, Wall = 1, Up = 2, Down = 3, Left = 4, Right = 5, None = 0} ScreenObjects; //Enum for snake parts buffer array.

int DoGame(void); //Run a game step.
int Score, Lives, AddLength = 5, IsAlive = FALSE, Difficulty = 0;
int ScreenInitialX, ScreenInitialY;
int ScreenX, ScreenY;
int SnakeX, SnakeY;
int SnakeOldX, SnakeOldY;
int FruitX = 0, FruitY = 0;

ScreenObjects Moving = None, NextMove = None, DirectionBuffer; //Using the up,down,left,right enum values here.
int KeyboardCharacter;

ScreenObjects *SnakeMoveBuffer;

int main()
{
    srand(time(NULL)); //Start randomization.
    initscr(); //Screen init.
    noecho(); //Don't echo anything to the terminal.
    nodelay(stdscr, TRUE); //Don't slow shit down!
    curs_set(FALSE); //No cursor.
    keypad(stdscr, TRUE); //Get keys, too.

    getmaxyx(stdscr, ScreenInitialY, ScreenInitialX);

    if (ScreenInitialX > 10 && ScreenInitialY > 10) {
        start_color(); //Start color for ncurses.
        clear(); //Clear screen.
        init_pair(1, COLOR_BLACK, COLOR_GREEN); //Color pairs.
        init_pair(2, COLOR_BLACK, COLOR_RED);
        init_pair(3, COLOR_GREEN, COLOR_BLACK); //Color pairs.
        attron(COLOR_PAIR(1));

        SnakeMoveBuffer = malloc((ScreenInitialX + 1) * (ScreenInitialY + 1) * (sizeof(ScreenObjects))); //Get snake-movement buffer.

        for (ScreenX = 1; ScreenX < ScreenInitialX - 1; ScreenX++) { //Draw horiz borders.
            mvprintw(0, ScreenX, BorderCharacter);
            mvprintw(ScreenInitialY - 1, ScreenX, BorderCharacter);
            SnakeMoveBuffer[ScreenX] = Wall; //Write in wall hit data for top border.
            SnakeMoveBuffer[((ScreenInitialY - 1)*ScreenInitialX) + ScreenX] = Wall; //Write in wall hit data for top border.
        }
        for (ScreenY = 0; ScreenY < ScreenInitialY; ScreenY++) { //Draw vert borders.
            mvprintw(ScreenY, 0, BorderCharacter);
            mvprintw(ScreenY, ScreenInitialX - 1, BorderCharacter);
            SnakeMoveBuffer[ScreenY * ScreenInitialX] = Wall; //Write in wall hit data for top border.
            SnakeMoveBuffer[(ScreenY * ScreenInitialX) + ScreenInitialX - 1] = Wall; //Write in wall hit data for top border.
        }

        SnakeX = ScreenInitialX / 2; //Set initial position for snake!
        SnakeY = ScreenInitialY / 2;
        SnakeOldX = SnakeX; //Tail is the same location as our current head on the waiting screen.
        SnakeOldY = SnakeY;
        while (DoGame()) { //While we don't have a break from our game function.
            refresh(); //Put the screen the game generated up.
            usleep(16000); //Sleep for a while.
        }
        free(SnakeMoveBuffer); //Won't need this since the game is done.
    } else printf("Unable to start game. Bigger terminal required!\n");
    endwin(); //Kill ncurses.
    return 0; //Quit the program.
}

int DoGame(void) //Does a game "step"
{
    getmaxyx(stdscr, ScreenY, ScreenX); //Record current X and Y screen values.
    if (ScreenY != ScreenInitialY || ScreenX != ScreenInitialX) return 0; //Check if terminal size has changed. If it has, quit the game.

    while ((KeyboardCharacter = getch()) != ERR) { //Read all keys.
        if (KeyboardCharacter == KEY_BACKSPACE) return 0; //Break program when backspace is pressed.
        if (KeyboardCharacter == KEY_UP && Moving != Down) NextMove = Up; //Move that way if okay.
        if (KeyboardCharacter == KEY_DOWN && Moving != Up) NextMove = Down; //Move that way if okay.
        if (KeyboardCharacter == KEY_LEFT && Moving != Right) NextMove = Left; //Move that way if okay.
        if (KeyboardCharacter == KEY_RIGHT && Moving != Left) NextMove = Right; //Move that way if okay.
        flushinp(); //Flush all input after the first one we read.
    }

    attron(COLOR_PAIR(3)); //Snake color first.
    mvprintw(SnakeY, SnakeX, SnakeHead); //Print head every time so we can see it pre-game start.
    mvprintw(0, 0, "Score:%i", Score);
    if (NextMove != None && IsAlive == FALSE && Difficulty == 0) {
        SnakeMoveBuffer[(SnakeY * ScreenInitialX) + SnakeX] = NextMove; //Write where the next body character is going to be.
        if (NextMove == Left || NextMove == Right) mvprintw(SnakeY, SnakeX, SnakeBodyLR);
        if (NextMove == Up || NextMove == Down) mvprintw(SnakeY, SnakeX, SnakeBodyUD);

        if (AddLength == 0) { //If we have no length to add to the snake.
            DirectionBuffer = SnakeMoveBuffer[(SnakeOldY * ScreenInitialX) +
                                              SnakeOldX]; //Look at the tail information (UDLR) to where to assign the new old tail at after we've removed this one.
            mvprintw(SnakeOldY, SnakeOldX, BlankCharacter); //Write blank character to old tail spot.
            SnakeMoveBuffer[(SnakeOldY * ScreenInitialX) + SnakeOldX] = Clear; //Clear the snake tail space from out hit buffer.
            if (DirectionBuffer == Up) SnakeOldY--; //Move buffers according to the direction information we had saved in our buffer.
            if (DirectionBuffer == Down) SnakeOldY++;
            if (DirectionBuffer == Right) SnakeOldX++;
            if (DirectionBuffer == Left) SnakeOldX--;
        } else Score++, AddLength--; //Else, skip deleting the tale this itteration.

        if (NextMove == Up) SnakeY--; //Move snake to new spot.
        if (NextMove == Down) SnakeY++;
        if (NextMove == Left) SnakeX--;
        if (NextMove == Right) SnakeX++;
        Moving = NextMove;

        if ((SnakeX == FruitX && SnakeY == FruitY) || FruitY == 0) { //If we hit fruit with our head, or it doesn't have a location yet.
            if (FruitY) AddLength += AddLen; //Add 3 length if we hit fruit.
            while ((SnakeX == FruitX && SnakeY == FruitY) || FruitY == 0) { //Use while because if we put fruit on our head again, it'll re-try a new location on the screen.
                FruitY = (rand() % (ScreenInitialY - 2)) + 1;
                FruitX = (rand() % (ScreenInitialX - 2)) + 1;
            }
        }
        if (SnakeMoveBuffer[(SnakeY * ScreenInitialX) + SnakeX]) IsAlive = TRUE; //If we have a body part, or a wall, we will have a number not 0. So if this is true, we are a dead snake.
        Difficulty = SnakeSpeed;
    } else if (NextMove != None) Difficulty--; //If moving, yet not time to update, chunk the counter.
    attron(COLOR_PAIR(2)); //Text color.
    if (FruitY) mvprintw(FruitY, FruitX, FruitChar); //Print fruit only if we have a location assigned above 0 Y.
    return 1;
}
