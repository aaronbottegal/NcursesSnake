#!/bin/sh

rm -rf ./build
meson ./ ./build
cd ./build
echo "Building program..."
ninja
mv ./NcursesSnake ../
cd ../